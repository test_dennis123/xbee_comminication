#include "Xbee_comminication.h"
#include "Arduino.h"
#include "solenoid.h"

static Xbee_comminication::Xbee_comminication()                  // Interruot function
	{ 
	} 
void static processRxPacket(ZBRxResponse& rx, uintptr_t)
    {
        ////Debug//Serial.print("Received packet from ");
        printHex(DebugSerial, rx.getRemoteAddress64());
        //Debug//Serial.println();

        Buffer buffer(rx.getData(),rx.getDataLength());
        char firstLetter   = buffer.remove<char>();
        char secondLetter  = buffer.remove<char>();
        // python 傳 character 的 byte arrays 時會多出兩個bytes 我拿不掉
        buffer.remove<char>();   
        buffer.remove<char>();
        float milliSeconds = buffer.remove<float>();
       
		float solenoid_valve_num = buffer.remove<float>();
		float Is_going_sleep=buffer.remove<float>();
		float sleeping_time=buffer.remove<float>();
		float sending_frequency_assign=buffer.remove<float>();
		sending_frequency_assign *=1000; //convert s to ms 
        if(firstLetter == 'A' && secondLetter == 'A')
	        {
	            //Debug//Serial.print("Watering for  in class!!");
	            //Debug//Serial.print(milliSeconds);
	            //Debug//Serial.println();
	            
	            //Debug//Serial.print("solenoid_valve_num=");
	            //Debug//Serial.println(solenoid_valve_num);
	            
	            //Debug//Serial.print("sending_frequency_assign=");
	            //Debug//Serial.println(sending_frequency_assign);
	            //Debug//Serial.print("Is_going_sleep=");
	            //Debug//Serial.println(Is_going_sleep);
	            //Debug//Serial.print("sleeping_time=");
	            //Debug//Serial.println(sleeping_time);
	            Xbee_comminication::repeats(solenoid_valve_num,milliSeconds,Is_going_sleep,sleeping_time,sending_frequency_assign);
				//這裡要想辦法放澆水函數 
	            // 放澆水 Function
	        } 
    }
void Xbee_comminication::SendData(char typecode1,char typecode2,float soilMoisture,float soilTemperature,float soilCO2,XBeeAddress64  addr64,float battery_voltage)
        {
            ZBTxRequest txRequest;
            txRequest.setAddress64(addr64);
            AllocBuffer<18> packet;
            packet.append<char>(typecode1);
            packet.append<char>(typecode2);
            packet.append<float>(soilMoisture);
            packet.append<float>(soilTemperature);
            packet.append<float>(soilCO2);
            packet.append<float>(battery_voltage);
            //Debug//Serial.print("in_solenoid_valve_num_working=");   
        	//Debug//Serial.println(soilCO2);
            txRequest.setPayload(packet.head, packet.len());
            uint8_t status = xbee.sendAndWait(txRequest,5000);
            if(status == 0) 
                {
                    DebugSerial.println("Successfully sent packet!");
                }
            else
                {
                    DebugSerial.print(F("Failed to send packet. Status: 0x"));
                    DebugSerial.println(status,HEX);
                }
        }
void Xbee_comminication::SendData(char typecode1,char typecode2,float soilMoisture,float soilTemperature,float soilCO2,XBeeAddress64  addr64,float battery_voltage,float light_voltage)
        {
            ZBTxRequest txRequest;
            txRequest.setAddress64(addr64);
            AllocBuffer<22> packet;
            packet.append<char>(typecode1);
            packet.append<char>(typecode2);
            packet.append<float>(soilMoisture);
            packet.append<float>(soilTemperature);
            packet.append<float>(soilCO2);
            packet.append<float>(battery_voltage);
            packet.append<float>(light_voltage);
            //Debug//Serial.print("in_solenoid_valve_num_working=");   
        	//Debug//Serial.println(soilCO2);
            txRequest.setPayload(packet.head, packet.len());
            uint8_t status = xbee.sendAndWait(txRequest,5000);
            if(status == 0) 
                {
                    DebugSerial.println("Successfully sent packet!");
                }
            else
                {
                    DebugSerial.print(F("Failed to send packet. Status: 0x"));
                    DebugSerial.println(status,HEX);
                }
        }
void Xbee_comminication::setup_recive_data()
	{
		DebugSerial.begin(115200);
        //Debug//Serial.println(F("Starting..."));
        XBeeSerial.begin(9600);
        xbee.begin(XBeeSerial);
        delay(100);
        xbee.onZBRxResponse(processRxPacket);
	}
void Xbee_comminication::setup_send_data()
	{
		
		DebugSerial.begin(115200);
		////////Debug//Serial.println(F("Starting..."));
		XBeeSerial.begin(9600);
		xbee.begin(XBeeSerial);
		delay(100);
	}
