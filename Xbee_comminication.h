#include "Arduino.h"
#include <AltSoftSerial.h>
#include <XBee.h>
#include <Printers.h>
#include "binary.h"
#define DebugSerial Serial                      // CALC          - The Debug Serial (using hardware serial port)
#define XBeeSerial  SoftSerial 
class Xbee_comminication   
		{
			public:       
			Xbee_comminication ();
			XBeeWithCallbacks xbee;                         // CALC          - The object used to interact with xbee module
			AltSoftSerial SoftSerial;                       // CALC          - The object used to interact with soft serial
			
			int Period = 3000;    
			//-------------------------------------------上面是共通變數 
			
			
			unsigned long last_tx_time = 0;                 // CALC          - Used to check the time
			static void repeat();
			void setup_send_data();
		    void setup_recive_data();
		    int add_func(void (*func_ptr)(int));
			static void repeats(int solenoid_valve_num,int sec,int Is_going_to_sleep,int watch_dog_time,int sending_frequency_assign);
			void sending_frequency_set(long unsigned Time_left_to_sleep,int period);
			
			//-----------------------------------------------------------下面是送資料用的 
			unsigned long last_tx_time_for_SendData = 0;                 // CALC          - Used to check the time
			void SendData(char typecode1,char typecode2,float soilMoisture,float soilTemperature,float soilCO2,XBeeAddress64  addr64,float battery_voltage);
			void SendData(char typecode1,char typecode2,float soilMoisture,float soilTemperature,float soilCO2,XBeeAddress64  addr64,float battery_voltage,float light_voltage);

			long unsigned last_time_send=0,sending_frequency=4000;
		}; 
