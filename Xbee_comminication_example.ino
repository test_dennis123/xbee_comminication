/*
 * Xbee_comminication.h
Description：進行通訊用的函式庫
function name：setup_recive_data()
        Description：初始化接收資料函式庫
        Syntax： Xbee_comminication_Rx.setup_recive_data();
        Parameters：non
        Returns：non
function name：setup_send_data()
        Description：初始化發送資料函式庫
        Syntax： Xbee_comminication_Tx.setup_send_data();
        Parameters：non
        Returns：non
function name：loop();
        Description：在loop內使用該函數，可以持續啟動通訊用函式庫
        Syntax： Xbee_comminication_Rx.xbee.loop();
        Parameters：non
        Returns：non

        
*/

#include "Xbee_comminication.h"
Xbee_comminication Xbee_comminication_Rx;
Xbee_comminication Xbee_comminication_Tx;
void setup() 
    {
        Xbee_comminication_Rx.setup_recive_data();
        Xbee_comminication_Tx.setup_send_data();
    }

void loop() 
    {
        Xbee_comminication_Rx.xbee.loop();
    }
static void Xbee_comminication::repeats(int solenoid_valve_num,int sec,int power_on_time_assign,int power_down_time_assign,int sending_frequency_assign)   
    {       
       
    }  //註冊當收到封包後，要進行的事情
